<br/>
 
<p align="center">
  <img src="./.assets/logos/ux.png" alt="Logo" width="200" height="200">
</p>
 
# Chapter UX
 
O Chapter UX tem como missão minimizar a taxa de erros, maximizar a produtividade e a satisfação dos usuários.
 
## Conteúdo
 
* [Entendendo mais](#entendendo-mais)
* [Processo criativo](#processo-criativo)
  * [Double Diamond](#double-diamond)
  * [Style Guide](#style-guide)
    * [Paleta de Cores](#paleta-de-cores)
    * [Tipografia](#tipografia)
    * [Hierarquia Visual](#hierarquia-visual)
    * [Design Atômico](#design-atômico)
* [Construção](#construção)
  * [Protótipo de baixa fidelidade](#protótipo-de-baixa-fidelidade)
  * [Protótipo de alta fidelidade](#protótipo-de-alta-fidelidade)
* [Links Úteis](#links-úteis)
  * [Ferramentas](#ferramentas)
  * [Design System](#design-system)
  * [Inspirações](#inspirações)
* [Contribuições](#contribuições)
 
## Entendendo mais
 
Quando utilizamos o termo UX, estamos falando da *User Experience* ou Experiência do Usuário, no nosso caso a experiência do usuário em utilizar as nossas aplicações. A forma que temos para comunicar com os usuários são as nossas interfaces e com isso, não podemos falar de UX sem falar da UI, também conhecida como *User Interface* ou Interface do Usuário. Enquanto a UX trabalha com a parte comportamental a UI trabalha com a parte visual.
 
Por mais que as duas sejam distintas é necessário que elas sempre estejam alinhadas, pois por mais que uma UI possa deixar os usuários boquiabertos, se a UX não for de igual padrão esse usuário logo deixará de usar a aplicação e com isso podemos ter consequências que podem decidir o futuro de uma empresa.
 
## Processo criativo
 
Boas interfaces não são construídas da noite pro dia. É muito importante entender que as interfaces devem estar em constante mudança, vide grandes empresas que alteram os seus produtos com frequência, seja para aumentar a usabilidade, melhorar aparência ou seguir tendências.
 
<p align="center">
  <img src="./.assets/images/evolucao-google.gif" alt="Evolução Google" width="50%"/>
</p>
 
### Double Diamond
 
A boa notícia é que ao contrário de algumas grandes empresas não precisamos esperar uma eternidade para entendermos melhor nosso projeto e criarmos boas interfaces, só precisamos de métodos consolidados, boas referências e algumas ferramentas. Sendo assim, vamos usar um método muito conhecido e amplamente utilizado no design, o *Double Diamond* ou Diamante Duplo, que consiste em **Entender, Definir, Prototipar e Validar**.
 
<p align="center">
  <img src="./.assets/images/double-diamond.png" alt="Diamante Duplo" width="50%"/>
</p>
 
Como podemos ver na imagem acima, temos um primeiro diamante, nessa etapa devemos explorar, ou seja, devemos entender o nosso problema, conhecer nossos usuários e extrair o máximo de informações que conseguimos, com isso definimos e separamos o que se encaixa melhor no escopo do nosso projeto. Após extrairmos só o que faz sentido, chegamos no segundo diamante, essa é a etapa da criação onde devemos desenvolver o nosso protótipo e então validar. Não necessariamente teremos um protótipo validado na última etapa, caso isso ocorra, devemos voltar para o início do segundo e algumas vezes até do primeiro diamante.
 
### Style Guide
 
O *Style Guide* ou Guia de Estilo é um conjunto de regras criadas para assegurar a consistência visual das nossas aplicações. Ele deve ser construído antes mesmo do protótipo de baixa fidelidade. Os integrantes que forem participar do processo de criação devem conhecer muito bem o projeto, por isso a necessidade de se utilizar métodos como o **Double Diamond**, além disso é sempre bom acompanhar as tendências e procurar por referências para deixar as interfaces mais atraente.
 
<p align="center">
  <img src="./.assets/images/guia-de-estilo-spotify.png" alt="Guia de Estilo do Spotify" width="50%"/>
</p>
 
É importante ressaltar que o Guia de Estilo deve ser conhecido por todo o Squad, ficando cada integrante responsável por criar um senso crítico e cobrar uns dos outros que as regras sejam aplicadas, além disso os integrantes de outros *Chapters* fora da UX passam a entender melhor as interfaces criadas.
 
#### Paleta de Cores
 
A utilização correta das cores é fundamental, pois elas são um dos pontos principais na identidade visual da aplicação, além de ser uma ótima ferramenta para deixar a experiência do usuário mais intuitiva, facilitando a navegação ou destacando algum elemento na tela. Para padronizar as cores de uma aplicação faremos uma Paleta de Cores, que é uma combinação intencional de cores, como as que podemos observar abaixo.
 
<p align="center">
  <img src="./.assets/images/paleta-cores.png" alt="Paleta de Cores" width="50%"/>
</p>
 
As cores em uma aplicação vão muito além do que só trazer beleza. Elas transmitem mensagens, emoções e até mesmo mudanças de humor. Por isso é bom conhecer a teoria das cores e entender como e onde aplicar cada uma, com isso criamos um olhar mais crítico na hora de fazer a escolha.
 
#### Tipografia
 
A Tipografia tem como estuda matéria de estudo as fontes, seja na criação e aplicação dos caracteres, estilos, formatos e arranjos visuais das palavras. As fontes ou tipos, são a base da comunicação escrita e devem ser adequados para transmitir a mensagem de forma correta e legível.
 
<p align="center">
  <img src="./.assets/images/tipografia.png" alt="Tipografia" width="50%"/>
</p>
 
Os tipos possuem três classificações primárias de estilo que são, sem serifa, com serifa e script. As fontes sem serifa são mais adequadas para mídias digitais, principalmente em títulos e corpo de textos, já as fontes com serifa são mais utilizadas em livros e em textos impressos.
 
<p align="center">
  <img src="./.assets/images/classificacoes-tipograficas.png" alt="Classificações Tipograficas" width="50%"/>
</p>
 
Cada estilo possui diversas famílias e cada família, por sua vez, possui algumas ou todas as variações **Thin, Light, Regular, Medium ou Semi Bold, Bold e Black ou Extra Bold**, além disso também temos as variação em Itálico.
 
#### Hierarquia Visual
 
ALém das cores e tipografia, precisamos também entender o conceito por trás da Hierarquia Visual, pois com ela conseguimos modificar a forma como o texto é lido. A Hierarquia Visual pode ser feita utilizando variações da mesma fonte, tamanhos, famílias e distâncias.
 
<p align="center">
  <img src="./.assets/images/hierarquia-visual.png" alt="Hierarquia Visual" width="50%"/>
</p>
 
As frases escritas com a fonte maior ou bold, normalmente, são lidas primeiro pois chamam mais a atenção do leitor. Na hora de projetar uma interface devemos levar isso em consideração, pois assim podemos dar ênfase em textos ou conteúdos mais importantes e minimizar os menos importantes.
 
#### Design Atômico
 
O Design Atômico é uma metodologia que dá sentido a componentes de uma interface. A ideia é fazer uma analogia ao comportamento dos átomos, moléculas e organismos. Por exemplo podemos ter botões e campos de entrada de dados, aqui definiremos como os átomos, se conectarmos esses átomos conseguimos criar os formulários, que definiremos como moléculas, por fim quando integramos essa molécula a outras, criamos um template que dará origem às nossas páginas que definiremos como organismo.
 
<p align="center">
  <img src="./.assets/images/design-atomico.png" alt="Design Atômico" width="50%"/>
</p>
 
Quando criamos componentes estamos também colaborando com os desenvolvedores, visto que se o componente for desenvolvido da maneira correta podemos obter vários ganhos de acordo com a engenharia de software, como encapsulamento, fraco acoplamento e reuso. Ou seja, conseguimos reutilizar esses componentes com segurança no nosso ambiente de desenvolvimento ou em similares ao do nosso projeto.
 
## Construção
 
A construção das interfaces auxiliam o Squad a entender o que pode dar certo ou não, ainda na fase inicial do projeto. Algumas características idealizadas podem não funcionar na prática. É por isso que os protótipos, principalmente os de alta fidelidade, podem ser testados por usuários antes do desenvolvimento, mostrando pontos reais de melhoria. 
 
> Durante todo o processo de desenvolvimento das interfaces é importante ter em mente que você não é o usuário. Então, construa para eles não para você!
 
### Protótipo de baixa fidelidade
 
Os prototipos de baixa fidelidade, são *sketches* ou rascunhos. É recomendável que seja desenhado à mão utilizando papel, lápis e borracha. Este tipo de protótipo deve conter o menor número de detalhes possíveis para representar os componentes utilizamos somente quadrados riscados representando o possível local de um componente. Esse tipo de protótipo também é conhecido como *Wireframe*.
 
<p align="center">
  <img src="./.assets/images/prototipo-baixa-fidelidade.png" alt="Design Atômico" width="50%"/>
</p>
 
A intenção desse protótipo é definir de modo simples a interação do usuário com o projeto sem preocupações com o design, sendo utilizado para auxiliar na definição do projeto e levantamentos de requisitos.
 
### Protótipo de alta fidelidade
 
Os protótipos de alta fidelidade, são representações interativas do produto, replicadas em computadores ou dispositivos móveis. Eles possuem características idênticas ou similares à interface real, tanto em detalhes quanto em funcionalidades. É aqui que colocamos em prática todos os conceitos apresentados.
 
<p align="center">
  <img src="./.assets/images/prototipo-alta-fidelidade.png" alt="Design Atômico" width="50%"/>
</p>
 
Para desenvolver os protótipos de alta fidelidade, vamos utilizar o **Figma** uma ferramenta de prototipação que permite a criação de protótipos interativos, possibilidade de desenvolver interfaces de forma colaborativa, entre outras funcionalidades.
 
## Links Úteis
 
Alguns links úteis para o desenvolvimento das interfaces.
 
### Ferramentas
 
- [Figma](https://figma.com)
- [Google Fonts](https://fonts.google.com/)
- [Color Tool](https://material.io/resources/color)
 
### Design System
 
- [Material](https://material.io/)
- [Fluent](https://www.microsoft.com/design/fluent/)
- [Apple](https://developer.apple.com/design/resources/)
 
### Inspirações
 
- [Spotify Design](https://spotify.design/)
- [Dribbble](https://dribbble.com/)
- [Awwwards](https://www.awwwards.com/)
- [GoodUI](https://goodui.org/)
 
## Contribuições
 
Gustavo Faria [@gustavofariaa](https://gitlab.com/gustavofariaa)
